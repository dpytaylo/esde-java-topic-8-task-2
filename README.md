# ESDE Java Topic 8 Task 2
**Task description:**<br>
Create a Test2 class with two variables. Add a constructor with input parameters. Add a constructor that initializes the default class members. Add set- and get- methods for the instance fields of the class.