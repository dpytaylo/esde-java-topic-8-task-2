package src;

public class Task2 {
    private String a;
    private String b;

    public Task2(String a, String b) {
        this.setA(a);
        this.setB(b);
    }

    public Task2() {
        this("", "");
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }
}
