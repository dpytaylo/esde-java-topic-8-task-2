package src;

public class Main {
    public static void main(String[] args) {
        var task = new Task2();

        System.out.println("task.getA() = '" + task.getA() + "'");
        System.out.println("task.getB() = '" + task.getB() + "'");

        task.setA("apple");
        System.out.println("task.getA() = " + task.getA());

        task.setB("banana");
        System.out.println("task.getB() = " + task.getB());

        var task2 = new Task2("dog", "cat");
        System.out.println("task2.getA() = " + task2.getA());
        System.out.println("task2.getB() = " + task2.getB());
    }
}